#include <QCoreApplication>

#include <iostream>
#include <unistd.h>

#include <QDebug>
#include <QRegExp>
#include <QDateTime>
#include <QtSql>
#include <QFileInfo>

#define _BSD_SOURCE

#include <stdio.h>
#include <errno.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#define PORT    8899
#define MAXMSG  102400


/**
 *  The MemoryStatistics struct keeps the information about the memory at a certain moment.
 */
typedef struct {
    QDateTime timestamp;
    unsigned int totalMemory;
    unsigned int usedMemory;
    unsigned int freeMemory;
    unsigned int sharedMemory;
    unsigned int buffersMemory;
    unsigned int cachedMemory;
} MemoryStatistics;

/**
  * The ProcessesStatistics structure keeps the information about the users and the active processes.
  */
typedef struct {
    QDateTime timestamp;
    QString user;
    unsigned int numberOfProcesses;

} ProcessesStatistics;

/*
 *  The CpuStatistics structure keeps the data about the CPU.
 */
typedef struct {
    QDateTime timestamp;
    QString architecture;
    QString CPUopMode;
    QString ByteOrder;
    QString VendorID;
    QString onLineCPUs;
    int numberOfCPUs;
    int threadPerCore;
    int corePerSocket;
    int sockets;
    int numaNodes;
    int CPUfamily;
    int model;
    int stepping;
    float CPUmhz;
}CpuStatistics;

/*
 * The UserStatistics structure keeps the data about the users logged.
 */
typedef struct {
    QString user;
    QString tty;
    QDateTime loginAt;
    QDateTime timestamp;
    QString comment;
} UsersStatistics;

/**
 * @brief runCommand : This function runs a given shell command regarding a computer's usage at a specific moment.
 * @param command : This represents the shell command to be executed.
 * @return command's execution output as a QString.
 */
QString runCommand(char* command){

    FILE *filePointer;
    char line[2000];
    char *commandOutput = (char*) malloc(sizeof(char) * 1024);

     // Open the command for reading.
     filePointer = popen(command, "r");
     if (filePointer == NULL) {
       printf("Failed to run command\n" );
       exit(1);
     }

     // Reads the output a line at a time and adds it in the commandOutput buffer.
     while (fgets(line, sizeof(line)-1, filePointer) != NULL){
       strcat(commandOutput, line);
       //printf("%s", line);
     }

     // close
     pclose(filePointer);

     return commandOutput;
}

/**
 * This function calls 'free -m' system command that gives information about computer's memory and its usage at a specific moment.
 * @brief runMemoryCommand
 * @return the retrieved information as a MemoryStatistics.
 */
MemoryStatistics runMemoryCommand(QSqlDatabase db) {

    // .*(total).*(used).*(free).*(shared).*(buffers).*(cached)
    QString totalString, usedString, freeString, sharedString, buffersString, cachedString;
    int total, used, free, shared, buffers, cached;

    // The commandOutput QSting will store the information returned from executing "free -m" command.
    QString commandOutput = runCommand("free -m");
    qDebug() << commandOutput;

    // A regex is used in order to match all the fields necesarry in order to  populate the memory structure.
    QRegExp regexp("Mem\\:\\s*(\\d+)\\s*(\\d+)\\s*(\\d+)\\s*(\\d+)\\s*(\\d+)\\s*(\\d+)");
    // The results of matching the regex are returned in a list of Qstrings, therefore the data needs to be converted into integer.
    int pos = regexp.indexIn(commandOutput);
    if (pos > -1) {
        qDebug() << "Captured texts: " << regexp.capturedTexts();

        totalString = regexp.cap(1);
        total = totalString.toInt();
        qDebug() << "Group 1: " << totalString << "Total: " << total;

        usedString = regexp.cap(2);
        used = usedString.toInt();
        qDebug() << "Group 2: " << regexp.cap(2) << "Used: " << used;

        freeString = regexp.cap(3);
        free = freeString.toInt();
        qDebug() << "Group 3: " << regexp.cap(3) << "Free: " << free;

        sharedString = regexp.cap(4);
        shared = sharedString.toInt();
        qDebug() << "Group 4: " << regexp.cap(4) << "Shared: " << shared;

        buffersString = regexp.cap(5);
        buffers = buffersString.toInt();
        qDebug() << "Group 5: " << regexp.cap(5) << "Buffers: " << buffers;

        cachedString = regexp.cap(6);
        cached = cachedString.toInt();
        qDebug() << "Group 6: " << regexp.cap(6) << "Cached: " << cached;
    } else {
        qDebug() << "No index in";
    }

    // The memory data structure is completed with the informations extracted at the currentDateTime().
    QDateTime currentTimestamp = QDateTime::currentDateTime();
    MemoryStatistics memoryStatistics;
    memoryStatistics.timestamp = currentTimestamp;
    memoryStatistics.totalMemory = total;
    memoryStatistics.usedMemory = used;
    memoryStatistics.cachedMemory = cached;
    memoryStatistics.buffersMemory = buffers;
    memoryStatistics.freeMemory = free;
    memoryStatistics.sharedMemory = shared;

    QString timestampString = memoryStatistics.timestamp.toString("yyyy-MM-dd HH:mm:ss");

    // Insert the value in the db
    QString insertMemoryStatsQuery = QString("INSERT INTO mem_stats (total, buffers, cached, free, shared, used, timestamp)"
                                               "VALUES (%1, %2, %3, %4, %5, %6, '%7')").arg(totalString, buffersString, cachedString,
                                                                                            freeString, sharedString, usedString, timestampString);

    // Run the query against the database
    db.exec(insertMemoryStatsQuery);

    qDebug() << db.lastError();

    return memoryStatistics;
}

/**
 * @brief runProcessesCommand This function calls 'ps hax -o user | sort | uniq -c' system command and stores information about the running processes in the db.
 */
void runProcessesCommand(QSqlDatabase db) {
    // CurrentTimestamp keeps the current date time.
    QDateTime currentTimestamp = QDateTime::currentDateTime();
    // Converting the timestamp into a string in order to introduce it into the database.
    QString currentTimestampString = currentTimestamp.toString("yyyy-MM-dd HH:mm:ss");

    unsigned int numberOfProcessesPerUser;
    QString userString, numberOfProcessesString;
    ProcessesStatistics processesStatistics;

    processesStatistics.timestamp = currentTimestamp;
    QString commandOutput = runCommand("ps hax -o user | sort | uniq -c");
    qDebug() << commandOutput;

     QRegularExpression regexp("\\s(\\d+)\\s+(\\w+)", QRegularExpression::MultilineOption);

     QRegularExpressionMatchIterator iterator = regexp.globalMatch(commandOutput);
     while (iterator.hasNext()) {
         QRegularExpressionMatch match = iterator.next();
         if (match.hasMatch()) {
              qDebug() << "User: " << match.captured(2);
              userString = match.captured(2);
              processesStatistics.user = userString;

              qDebug() << "Number of processes: " << match.captured(1);
              numberOfProcessesString = match.captured(1);
              numberOfProcessesPerUser = numberOfProcessesString.toInt();

              processesStatistics.numberOfProcesses = numberOfProcessesPerUser;


              // Inserting the data into the db.
              QString insertProcessesQuery = QString("INSERT INTO process_stats"
                                                     "(active_processes, pcuser, timestamp)"
                                                     "VALUES (%1, '%2', '%3')").arg(numberOfProcessesString, userString, currentTimestampString);
              db.exec(insertProcessesQuery);
              qDebug() << db.lastError();
         }
     }
}

/**
 * @brief runCpuCommand This function calls 'lscpu' command and fills in the database with information about the CPU.
 */
void runCpuCommand(QSqlDatabase db) {
    QDateTime currentTimeStamp = QDateTime::currentDateTime();
    QString currentTimeStampString = currentTimeStamp.toString("yyyy-MM-dd HH:mm:ss");
    CpuStatistics cpuStatistics;
    cpuStatistics.timestamp = currentTimeStamp;

    // The data is stored in QStrings in order to insert it into the database.
    QString architectureString, CPUopModeString, ByteOrderString, numberOfCPUsString, onLineCPUsString, threadPerCoreString;
    QString corePerSocketString, socketsString, numaNodesString, VendorIDString, CPUfamilyString, modelString, steppingString, CPUmhzString;


    QString commandOutput = runCommand("lscpu");
    QRegularExpression regexp(".*\\:\\s+(.*)");

    QRegularExpressionMatchIterator iterator = regexp.globalMatch(commandOutput);
    int matchOnRow = 0;
    while (iterator.hasNext()) {
        QRegularExpressionMatch match = iterator.next();
        if (match.hasMatch()) {
            switch (matchOnRow) {
                case 0:
                    qDebug() << "Architecture: " << match.captured(1);
                    architectureString = match.captured(1);
                    cpuStatistics.architecture = architectureString;
                    break;
                case 1:
                    qDebug() << "CPU op-mode(s): " << match.captured(1);
                    CPUopModeString = match.captured(1);
                    cpuStatistics.CPUopMode = CPUopModeString;
                    break;
                case 2:
                    qDebug() << "Byte Order: " << match.captured(1);
                    ByteOrderString = match.captured(1);
                    cpuStatistics.ByteOrder = ByteOrderString;
                    break;
                case 3:
                    qDebug() << "CPU(s): " << match.captured(1);
                    numberOfCPUsString = match.captured(1);
                    cpuStatistics.numberOfCPUs = numberOfCPUsString.toInt();
                    break;
                case 4:
                    qDebug() << "On-line CPU(s) list: " << match.captured(1);
                    onLineCPUsString = match.captured(1);
                    cpuStatistics.onLineCPUs = onLineCPUsString;
                    break;
                case 5:
                    qDebug() << "Thread(s) per core: " << match.captured(1);
                    threadPerCoreString = match.captured(1);
                    cpuStatistics.threadPerCore = threadPerCoreString.toInt();
                    break;
                case 6:
                    qDebug() << "Core(s) per socket: " << match.captured(1);
                    corePerSocketString = match.captured(1);
                    cpuStatistics.corePerSocket = corePerSocketString.toInt();
                    break;
                case 7:
                    qDebug() << "Socket(s): " << match.captured(1);
                    socketsString = match.captured(1);
                    cpuStatistics.sockets = socketsString.toInt();
                    break;
                case 8:
                    qDebug() << "NUMA node(s): " << match.captured(1);
                    numaNodesString = match.captured(1);
                    cpuStatistics.numaNodes = numaNodesString.toInt();
                    break;
                case 9:
                    qDebug() << "Vendor ID: " << match.captured(1);
                    VendorIDString = match.captured(1);
                    cpuStatistics.VendorID = VendorIDString;
                    break;   
                case 10:
                    qDebug() << "CPU Family: " << match.captured(1);
                    CPUfamilyString = match.captured(1);
                    cpuStatistics.CPUfamily = CPUfamilyString.toInt();
                    break;
                case 11:
                    qDebug() << "Model: " << match.captured(1);
                    modelString = match.captured(1);
                    cpuStatistics.model = modelString.toInt();
                    break;
                case 12:
                    qDebug() << "Stepping: " << match.captured(1);
                    steppingString = match.captured(1);
                    cpuStatistics.stepping = steppingString.toInt();
                    break;
                case 13:
                    qDebug() << "CPU MHz: " << match.captured(1);
                    CPUmhzString = match.captured(1);
                    cpuStatistics.CPUmhz = CPUmhzString.toFloat();
                    break;

                default:
                    break;

            }
            matchOnRow++;
        }
    }
    QString insertCpuQuery1 = QString("INSERT INTO cpu_stats"
                                      "(workarhitecture, CPU_op_modes, byte_order, "
                                      "cpus, online_CPU_list, threads_per_core,"
                                      "cores_per_socket, sockets, NUMA_nodes, vendor_id, "
                                      "CPU_family, model, stepping, CPU_mhz, timestamp)"
                                      "VALUES ('%1', '%2', '%3', %4, '%5', %6, %7, ")
            .arg(architectureString, CPUopModeString, ByteOrderString, numberOfCPUsString,
                 onLineCPUsString, threadPerCoreString, corePerSocketString);

    QString insertCpuQuery2 = QString("%1, %2, '%3', %4, %5, %6, %7, '%8')")
            .arg(socketsString, numaNodesString, VendorIDString, CPUfamilyString,
                 modelString, steppingString, CPUmhzString, currentTimeStampString);

    QString insertCpuQuery = insertCpuQuery1 + insertCpuQuery2;

    //qDebug() << insertCpuQuery;
    db.exec(insertCpuQuery);
    //qDebug() << db.lastError();
}
/**
 * @brief runUserStatisticsCommand Populates the db with information about the logged users.
 * @param db The database to be populated.
 * @return A structure containing data about the users and their connecting time.
 */
UsersStatistics runUserStatisticsCommand(QSqlDatabase db) {
    QDateTime timestamp = QDateTime::currentDateTime();
    QString currentTimeStampString = timestamp.toString("yyyy-MM-dd HH:mm:ss");
    UsersStatistics userStatistics;
    userStatistics.timestamp = timestamp;

    QString userString, loginAtString, ttyString, commentString;

    QString commandOutput = runCommand("who");
    qDebug() <<commandOutput;

    // Creating the regex.
    QRegularExpression regexp("(\\w+)\\s+(.*[^ ])\\s+(2.*)\\s(\\(.*\\))", QRegularExpression::MultilineOption);
    QRegularExpressionMatchIterator iterator = regexp.globalMatch(commandOutput);
    while (iterator.hasNext()) {
        QRegularExpressionMatch match = iterator.next();
        if (match.hasMatch()) {
            qDebug() << "User :" << match.captured(1);
            userString = match.captured(1);
            userStatistics.user = userString;

            qDebug() << "TTY : " << match.captured(2);
            ttyString = match.captured(2);
            userStatistics.tty = ttyString;

            qDebug() << "Login at : " << match.captured(3);
            loginAtString = match.captured(3);
            loginAtString = loginAtString + ":00";
            userStatistics.loginAt = QDateTime::fromString(loginAtString,"yyyy-MM-dd HH:mm:ss");

            qDebug() << "Comment : " << match.captured(4);
            commentString = match.captured(4);
            userStatistics.comment = commentString;
        }
        // Inserting information into the db.
        QString inserUsersQuery = QString("INSERT INTO user_stats"
                                          "(user_log, tty, login_at, timestamp, comment)"
                                          "VALUES('%1', '%2', '%3', '%4', '%5')").arg(userString, ttyString, loginAtString, currentTimeStampString, commentString);

        db.exec(inserUsersQuery);
        qDebug() << db.lastError();

    }

}


/**
 * @brief prepareDB Creates the DB and the tables.
 */
QSqlDatabase prepareDB()
{
    QSqlDatabase db = QSqlDatabase::addDatabase("QPSQL");
    db.setHostName("localhost");
    db.setDatabaseName("rc");
    db.setUserName("postgres");
    db.setPassword("rc");

    // Get the connection
    if (db.open()) {
        qDebug() << "DB ready for use";
    } else {
        qDebug() << "Could not connect to DB!";
    }

    // Create the table for the Memory Statistics
    QString createMemoryStatisticsTable("CREATE TABLE IF NOT EXISTS mem_stats ("
                                        "id SERIAL PRIMARY KEY,"
                                        "total INTEGER,"
                                        "buffers INTEGER,"
                                        "cached INTEGER,"
                                        "free INTEGER,"
                                        "shared INTEGER,"
                                        "used INTEGER,"
                                        "timestamp TIMESTAMP)");
    db.exec(createMemoryStatisticsTable);
    qDebug() << db.lastError();


    // Create the table for the Processes Statistics
    QString createProcessesStatisticsTable("CREATE TABLE IF NOT EXISTS process_stats ("
                                           "id SERIAL PRIMARY KEY,"
                                            "active_processes INTEGER,"
                                            "pcuser CHAR(30),"
                                            "timestamp TIMESTAMP)");
    db.exec(createProcessesStatisticsTable);
    qDebug() << db.lastError();


    // Create the table for the CPU statistics
    QString createCPUStatisticsTable("CREATE TABLE IF NOT EXISTS cpu_stats ("
                                                                            "id SERIAL PRIMARY KEY,"
                                                                            "workarhitecture TEXT,"
                                                                            "CPU_op_modes TEXT,"
                                                                            "byte_order TEXT,"
                                                                            "cpus INTEGER,"
                                                                            "online_CPU_list TEXT,"
                                                                            "threads_per_core INTEGER,"
                                                                            "cores_per_socket INTEGER,"
                                                                            "sockets INTEGER,"
                                                                            "NUMA_nodes INTEGER,"
                                                                            "vendor_id TEXT,"
                                                                            "CPU_family INTEGER,"
                                                                            "model INTEGER,"
                                                                            "stepping INTEGER,"
                                                                            "CPU_mhz REAL,"
                                                                            "timestamp TIMESTAMP);");
   db.exec(createCPUStatisticsTable);
   qDebug() << db.lastError();

   // Create the table for User Statistics.
   QString createUserStatisticsTable("CREATE TABLE IF NOT EXISTS user_stats("
                                     "id SERIAL PRIMARY KEY,"
                                     "user_log TEXT,"
                                     "tty TEXT,"
                                     "login_at TIMESTAMP,"
                                     "timestamp TIMESTAMP,"
                                     "comment TEXT);");
   db.exec(createUserStatisticsTable);
   qDebug() << db.lastError();

   return db;
}

/**
 * @brief gatherStatistics In this function the informations are gathered in a while loop.
 */
void gatherStatistics(QSqlDatabase db) {

    // Open the DB for stats gathering
    db.open();


    // The data extracted from this command is not often updated, this is why it is not needed to be in a while loop.
    runCpuCommand(db);

    while (1) {

        runMemoryCommand(db);
        runProcessesCommand(db);
        runUserStatisticsCommand(db);

        // At the end of the loop, sleep X seconds
        usleep(10 * 1000000); // seconds * microseconds
        qDebug() << "---";
    }
}


/**
 * @brief make_socket This function creates a socket.
 * @return The return value is a socket descriptor.
 */
int make_socket()
{
    // The server's structure.
    struct sockaddr_in server;
    // Socket descriptors.
    int socketDescriptor;
    // Option used for setsockopt().
    int optionValue=1;

    // Creating the socket.
    socketDescriptor = socket(AF_INET, SOCK_STREAM, 0);
    if (socketDescriptor == -1)
      {
        perror ("[server] Socket error.\n");
        return errno;
      }

    // Setting the SO_REUSEADDR for the socket.
    setsockopt(socketDescriptor, SOL_SOCKET, SO_REUSEADDR, &optionValue, sizeof(optionValue));

    // Preparing the data structures.
    bzero (&server, sizeof (server));

    // Filling in the server's structure.
    server.sin_family = AF_INET;
    // Converting any address into host to network long format.
    server.sin_addr.s_addr = htonl (INADDR_ANY);
    // Converting the port into host to network short, and adding it into the structure.
    server.sin_port = htons (PORT);

    // Attaching the socket.
    if (bind (socketDescriptor, (struct sockaddr *) &server, sizeof (struct sockaddr)) == -1)
      {
        perror ("[server] Bind() error.\n");
        return errno;
      }
  
  
    return socketDescriptor;
}

/**
 * @brief CScomunicate This function ensures the communication between the server and the client.
 * @param filedes This represents the file descriptor of the client.
 */
int ClientServerComunicate (int filedes, QSqlDatabase db)
{
  // The recieved message from the client.
  char message[MAXMSG];
  // The response mesage for the client.
  char responseMessage[MAXMSG];
  // The number of written/read octets.
  int octets;


  octets = read (filedes, message, MAXMSG);
  if (octets < 0)
    {
      perror ("Read() error from client.\n");
      return 0;
    }
  else if (octets == 0)
    // End of file.
    return -1;
  else
    {
      // Data read from client.
      fprintf (stderr, "Server: got message: `%s' from fd %d.\n", message, filedes);

      // Process the request

      /**** Response ****/

      // Erase the response buffer
      bzero(responseMessage, MAXMSG);

      // Transform the received query from the client into an executable QSqlQuery via a QString
      QSqlQuery query(db);
      QString textData;
      QString receivedQuery(message);
      bzero(message, MAXMSG);
      receivedQuery.remove('\b');

      // Run the query
      if(query.exec(receivedQuery))
      {
          qDebug() << QDateTime::currentDateTime() << "QUERY SUCCESS ";

          // Model provides a read-only data model for SQL result sets.
          QSqlQueryModel* model=new QSqlQueryModel();
          // Setting the query for the model.
          model->setQuery(query);


          // Getting the result rows and columns from query.exec()
          while (model->canFetchMore())
             model->fetchMore();
          int rows=model->rowCount();
          int columns=model->columnCount();

          // Transforming the output data from the table into a QString with csv format.
          for (int i = 0; i < rows; i++)
          {
              for (int j = 0; j < columns; j++)
              {
                  textData += model->data(model->index(i,j)).toString();

                  if (j != (columns-1)) {
                    textData += ", ";      // For .csv file format.
                  }
              }
              textData += "\n";             // For new line segmentation.
          }
      } else {
          // If the query could not be executed an error message will be sent to the client.
          textData += "Error, ";
          textData += query.lastError().text();
      }

      //qDebug() << query.lastError();

      qDebug() << "Results: " << textData;


      // Preparing the response message for the client.
      QByteArray byteArray = textData.toLatin1();
      const char *byteArrayData = byteArray.data();
      strcpy(responseMessage, byteArrayData);


      if(write(filedes, responseMessage, sizeof(responseMessage)));

      qDebug() << "Sent " << filedes << " message " << responseMessage;

      return 0;
    }
}

/**
 * @brief server : The server function serves the clients in a concurrent mode using the select function with its lists of active descriptors.
 */
void server(QSqlDatabase db)
{
        // Open the DB for the server.
        db.open();

        // The client's structure.
        struct sockaddr_in clientStructure;
        // The file descriptors reading list.
        fd_set readFdList;
        // List of the active file descriptors.
        fd_set activeFdList;
        // Time structure for select function.
        struct timeval timeStruct;
        // Socket descriptors.
        int clientDescriptor, socket;
        // Descriptor used for iterating through the file descriptors lists.
        int iteratorFd;
        // Maximum number of descriptors.
        int maxDescriptors;
        // The sockaddrLength of the sockaddr_in structure.
        unsigned int sockaddrLength;


        // Creating the socket for the server.
        socket = make_socket();

        if (listen (socket, 1) < 0)
          {
            perror ("Listen error!");
            exit(1);
          }

        // Completing the reading file descriptors list.
        // In the beginning, the list of active descriptors is empty.
        FD_ZERO (&activeFdList);
         // Adding the new socket into the active descriptors list.
        FD_SET (socket, &activeFdList);

       // The time structure is set to wait a second.
       timeStruct.tv_sec = 1;
       timeStruct.tv_usec = 0;

        /* The maximum value of the used descriptors. */
        maxDescriptors = socket;

        printf ("[server] Waiting at PORT : %d...\n", PORT);
        fflush (stdout);

        // Serving the clients concurrent.
        while (1)
          {
            // Adjusting the list of descriptors in use.
            bcopy ((char *) &activeFdList, (char *) &readFdList, sizeof (readFdList));

            // Using the select funtion.
            if (select (maxDescriptors + 1, &readFdList, NULL, NULL, &timeStruct) < 0)
        {
          perror ("[server] Select() error.\n");
          //return errno;
          exit(2);
        }

            // Checking if the socket is ready to accept the clients.
            if (FD_ISSET (socket, &readFdList))
        {
          // Preparing the structure for the client.
          sockaddrLength = sizeof(clientStructure);
          bzero (&clientStructure, sizeof (clientStructure));

          // Accepting the connection when a client arrives.
          clientDescriptor = accept (socket, (struct sockaddr *) &clientStructure, &sockaddrLength);

          // Client error.
          if (clientDescriptor < 0)
            {
              perror ("[server] Accept() error.\n");
              continue;
            }

         // Adjusting the maximum value of the descriptors.
         if (maxDescriptors < clientDescriptor) {
            maxDescriptors = clientDescriptor;
         }

          // Adding the new descriptor into the active descriptors list.
          FD_SET (clientDescriptor, &activeFdList);

          printf("[server] The client with the descriptor %d has connected.\n",clientDescriptor);
          fflush (stdout);
        }
            // Checking if there is any free socket client to send the answer.
            for (iteratorFd = 0; iteratorFd<= maxDescriptors; iteratorFd ++)  // Iterating through the descriptors list.
        {
          // Looking for a free reading socket.
          if (iteratorFd != socket && FD_ISSET (iteratorFd, &readFdList))
            {
              if (ClientServerComunicate(iteratorFd, db) < 0)
          {
            printf ("[server] The client with the descriptor : %d is disconnected .\n",iteratorFd);
            fflush (stdout);
            close (iteratorFd);
            // Taking out the descriptor from the active descriptors list.
            FD_CLR (iteratorFd, &activeFdList);

          }
            }
        }     /* for */
          }       /* while */
      }       /* main */


int main(int argc, char *argv[])
{
    // This is used in order to get an event loop for the app that uses QT but doesn't use a GUI.
    // It is needed for enabling the Postgre SQL DB connections.
    QCoreApplication a(argc, argv);

    // Prepare the DB structure
    QSqlDatabase db = prepareDB();

    int pid = fork();
    if(pid == -1) {
        qDebug() << "Fork error";
    } else if (pid == 0) {
            // Child
            gatherStatistics(db);
    }

    // Server implementation
    qDebug() << "Parent!";

    // Calling the server.
    server(db);

    return 0;
}



